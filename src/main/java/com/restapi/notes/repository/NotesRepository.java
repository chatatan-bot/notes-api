package com.restapi.notes.repository;

import java.util.List;

import com.restapi.notes.core.Notes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotesRepository extends JpaRepository<Notes, Long> {

    List<Notes> findByUserId(String userId);

    Notes findById(long id);
}
