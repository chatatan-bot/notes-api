package com.restapi.notes.controller;

import com.restapi.notes.core.Notes;
import com.restapi.notes.repository.NotesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class NotesController {

    @Autowired
    private NotesRepository notesrepository;

    @GetMapping("/")
    public String landing() {
        return "Hi, I am Chatatan's Notes API";
    }

    @PostMapping("/notes")
    public void createNotes(@RequestBody Notes notes) {
        notesrepository.save(notes);
    }

    @GetMapping("/retrieve/{userId}")
    public List<Notes> getNotes(@PathVariable(value = "userId") String userId) {
        return notesrepository.findByUserId(userId);
    }

    @GetMapping("/retrieveDetails/{id}")
    public String getNotesDetails(@PathVariable(value = "id") long id) {
        try {
            Notes notes = notesrepository.findById(id);
            return notes.getTitle() + "\n" + notes.getNotes();
        } catch (Exception e) {
            return "";
        }
    }

    @DeleteMapping("/delete/{id}")
    public String deleteNotes(@PathVariable(value = "id") long id) {
        try {
             Notes notes = notesrepository.findById(id);
             notesrepository.delete(notes);
             return String.format("Notes: %s deleted!", notes.getTitle());
        } catch (Exception e) {
             return "Worry not! The notes has been previously deleted :)";
        }
    }
}
