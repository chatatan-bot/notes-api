package com.restapi.notes.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "notes")
public class Notes {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @NotBlank
    @Column(name = "userId")
    private String userId;

    @NotBlank
    @Column(name = "title")
    private String title;

    @NotBlank
    @Column(name = "notes")
    private String notes;

    public Notes(String userId, String notes, String title) {
        this.userId = userId;
        this.notes = notes;
        this.title = title;
    }

    public Notes() {
        super();
    }

    public String getNotes() {
        return notes;
    }

    public String getUserId() {
        return userId;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setUserId(String userID) {
        this.userId = userID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
