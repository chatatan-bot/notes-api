# Chatatan Notes API

[![pipeline status](https://gitlab.com/chatatan-bot/notes-api/badges/master/pipeline.svg)](https://gitlab.com/chatatan-bot/notes-api/-/commits/master)
[![coverage report](https://gitlab.com/chatatan-bot/notes-api/badges/master/coverage.svg)](https://gitlab.com/chatatan-bot/notes-api/-/commits/master)

## About Chatatan

Chatatan is a LINE bot application that serves as a personal notes and schedule keeper. Check out our Chatatan LINE bot!

<a href="https://line.me/R/ti/p/%40045nhzyf"><img height="36" alt="Add Chatatan" src="https://scdn.line-apps.com/n/line_add_friends/btn/en.png"></a>

## Features

Chatatan Notes API is one of the four services in Chatatan. It acts as the API endpoint to retrieve data from Chatatan's Notes Feature.

|#|Path|Method|Return Type|Desc|
|---|:---|---|---|---|
|1| `notes`| **POST** |`void`| Add notes to database in the form of json object|
|2| `retrieve/{userId}`| **GET** |`List<Event>`| Get all user's note. | 
|3| `retrieveDetails/{id}`| **GET** |`String`| Get the details of the notes by id. |
|4| `delete/{id}`| **DELETE** |`String`| Delete a notes based on the id, and provide String response if the notes has been successfully deleted or if the notes is not found.. |


## Authors [B10]
* [Wulan Mantiri](https://gitlab.com/wulanmantiri_)
* [Naufal Hilmi Irfandi](https://gitlab.com/naufalirfandi)
* [Mohammad Hasan Albar](https://gitlab.com/muhammadalbr)
* [Gilbert Stefano Wijaya](https://gitlab.com/gilbertstefano48)
* [Bayukanta Iqbal Gunawan](https://gitlab.com/bayukanta)

## Acknowledgements
* CS UI - Advanced Programming B 2020
